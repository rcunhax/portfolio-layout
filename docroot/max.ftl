
<div class="columns-max" id="main-content" role="main">
    
    <div class="container-fluid border-top my-3 py-2"></div>
    
    <div class="container">
        <div class="portlet-layout row">
            <div class="col-md-12 portlet-column portlet-column-only" id="column-1">
                ${processor.processMax()}
            </div>
        </div>
    </div>
</div>