<div class="portfolio-layout" id="main-content" role="main">
	
	<div class="container">
		<div class="portlet-layout row">
			<div class="col-md-12 portlet-column portlet-column-first" id="column-1">
				${processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")}
			</div>
		</div>
	</div>
	
	<div class="container-fluid banner-wrapper">
		<div class="portlet-layout row">
			<div class="col-md-12 portlet-column portlet-column-first" id="column-2">
				${processor.processColumn("column-2", "portlet-column-content portlet-column-content-first")}
			</div>
		</div>
	</div>
	<div class="container">
		<div class="portlet-layout row">
			<div class="col-md-12 portlet-column portlet-column-first" id="column-3">
				${processor.processColumn("column-3", "portlet-column-content")}
			</div>
		</div>

		<div class="portlet-layout row">
			<div class="col-md-7 portlet-column portlet-column-first" id="column-4">
				${processor.processColumn("column-4", "portlet-column-content")}
			</div>
			<div class="col-md-5 portlet-column portlet-column-first" id="column-5">
				${processor.processColumn("column-5", "portlet-column-content")}
			</div>
		</div>

		<div class="portlet-layout row">
			<div class="col-md-12 portlet-column portlet-column-first" id="column-6">
				${processor.processColumn("column-6", "portlet-column-content")}
			</div>
		</div>

		<div class="portlet-layout row">
			<div class="col-md-5 portlet-column portlet-column-first" id="column-7">
				${processor.processColumn("column-7", "portlet-column-content")}
			</div>
			<div class="col-md-7 portlet-column portlet-column-first" id="column-8">
				${processor.processColumn("column-8", "portlet-column-content")}
			</div>
		</div>

		<div class="portlet-layout row">
			<div class="col-md-12 portlet-column portlet-column-first" id="column-9">
				${processor.processColumn("column-9", "portlet-column-content")}
			</div>
		</div>
		
	</div>
</div>